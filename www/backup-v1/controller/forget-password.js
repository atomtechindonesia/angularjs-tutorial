export default function($scope) {
    $scope.screenVisible = false
    $scope.hideNavbar = false
    $scope.title = "Forget Password"

    $scope.$on('screenVisible-forgetPassword', function() {
        $scope.screenVisible = true
    })

    $scope.onBack = function() {
        $scope.screenVisible = false
    }
}
