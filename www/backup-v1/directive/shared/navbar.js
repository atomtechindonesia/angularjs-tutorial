export default function() {
    return {
        scope: {
            onBack: '&',
            title: '=',
            hideNavbar: '='
        },
        templateUrl: 'view/shared/navbar.html'
    }
}