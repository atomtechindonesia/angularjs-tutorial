export default function() {
    return {
        transclude: true,
        templateUrl: 'view/shared/screen.html'
    }
}