import dirButton from '../directive/shared/button.js'
import dirInput from '../directive/shared/input.js'
import dirLink from '../directive/shared/link.js'
import dirNavbar from '../directive/shared/navbar.js'
import dirScreen from '../directive/shared/screen.js'

import screenDetail from '../directive/detail.js'
import screenHome from '../directive/home.js'
import screenLogin from '../directive/login.js'
import screenForgetPassword from '../directive/forget-password.js'

import conDetail from '../controller/detail.js'
import conHome from '../controller/home.js'
import conLogin from '../controller/login.js'
import conForgetPassword from '../controller/forget-password.js'

angular.module('app', [])

    .directive('tsButton', dirButton)
    .directive('tsInput', dirInput)
    .directive('tsLink', dirLink)
    .directive('tsNavbar', dirNavbar)
    .directive('tsScreen', dirScreen)

    .directive('screenDetail', screenDetail)
    .directive('screenHome', screenHome)
    .directive('screenLogin', screenLogin)
    .directive('screenForgetPassword', screenForgetPassword)

    .controller('detail', conDetail)
    .controller('home', conHome)
    .controller('login', conLogin)
    .controller('forgetPassword', conForgetPassword)
