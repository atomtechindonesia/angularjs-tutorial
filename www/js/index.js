const app = angular
    .module('app', ['ngRoute'])
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'view/login.html',
                controller:  'loginController'
            })
            .when('/home', {
                templateUrl: 'view/home.html',
                controller:  'homeController'
            })
            .when('/transaction/create', {
                templateUrl: 'view/transaction-create.html',
                controller:  'transactionCreateController'
            })
            .when('/transaction/view/:id', {
                templateUrl: 'view/transaction-view.html',
                controller:  'transactionViewController'
            })
            .otherwise({
                templateUrl: 'view/login.html',
                controller:  'loginController'
            })
    })
