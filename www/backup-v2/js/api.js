const baseAPIUrl = 'https://ts-angularjs.herokuapp.com/api'

const api = {
    Login(username, password) {
        return fetch(`${baseAPIUrl}/login`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({
                username,
                password
            })
        })
        .then(response => response.json())
        .then(json => {
            if (json.data != '') {
                localStorage.setItem('jwt', json.data)
                return true
            }
            return false
        })
    },
    TransactionGet() {
        return fetch(`${baseAPIUrl}/transaction`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'Content-Type': 'application/json'
            },
            method: 'GET'
        })
        .then(response => response.json())
        .then(json => json)
    },
    TransactionGetDetail(id) {
        return fetch(`${baseAPIUrl}/transaction/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'Content-Type': 'application/json'
            },
            method: 'GET'
        })
        .then(response => response.json())
        .then(json => json)
    },
    TransactionInsert(transaction_number, transaction_date, details) {
        return fetch(`${baseAPIUrl}/transaction`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({
                transaction_number,
                transaction_date,
                details
            })
        })
        .then(response => response.json())
        .then(json => json)
    },
    TransactionDelete(id) {
        return fetch(`${baseAPIUrl}/transaction/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
                'Content-Type': 'application/json'
            },
            method: 'DELETE'
        })
        .then(response => response.json())
        .then(json => json)
    }
}
