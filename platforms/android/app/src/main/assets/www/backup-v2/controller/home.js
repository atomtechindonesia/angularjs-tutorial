import api from '../js/api.js'

export default function($scope, $location) {
    $scope.init = function() {
    }

    $scope.handleBack = function() {
        $location.path('')
    }
    
    $scope.handleCreate = function() {
        $location.path('transaction/create')
    }
    
    $scope.handleView = function() {
        $location.path('transaction/view/1')
    }
}
