const baseAPIUrl = 'https://ts-angularjs.herokuapp.com/api'

function Login(username, password) {
    return fetch(`${baseAPIUrl}/login`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({
            username,
            password
        })
    })
    .then(response => response.json())
    .then(json => {
        if (json.data != '') {
            localStorage.setItem('jwt', json.data)
            return true
        }
        return false
    })
}

function TransactionGet() {
    return fetch(`${baseAPIUrl}/transaction`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
            'Content-Type': 'application/json'
        },
        method: 'GET'
    })
    .then(response => response.json())
    .then(json => json)
}

function TransactionGetDetail(id) {
    return fetch(`${baseAPIUrl}/transaction/${id}`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
            'Content-Type': 'application/json'
        },
        method: 'GET'
    })
    .then(response => response.json())
    .then(json => json)
}

function TransactionInsert(transaction_number, transaction_date, details) {
    return fetch(`${baseAPIUrl}/transaction`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({
            transaction_number,
            transaction_date,
            details
        })
    })
    .then(response => response.json())
    .then(json => json)
}

function TransactionDelete(id) {
    return fetch(`${baseAPIUrl}/transaction/${id}`, {
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`,
            'Content-Type': 'application/json'
        },
        method: 'DELETE'
    })
    .then(response => response.json())
    .then(json => json)
}

export default {
    Login,
    TransactionGet,
    TransactionGetDetail,
    TransactionInsert,
    TransactionDelete
}
