import RepoUsers from '../repository/users.js'

export default function($scope, $rootScope) {
    $scope.screenVisible = true
    $scope.hideNavbar = true
    $scope.loading = false
    $scope.form = {
        username: '1',
        password: 'Bret'
    }

    $scope.handleForgetPassword = function() {
        $rootScope.$broadcast("screenVisible-forgetPassword")
    }

    $scope.handleLogin = function() {
        $scope.loading = true
        RepoUsers.Login($scope.form.username, $scope.form.password)
            .then(res => {
                if (res.id > 0) {
                    $scope.$broadcast("screenVisible-home")
                } else {
                    alert('Login Failed')
                }
                $scope.loading = false
                $scope.$apply()
            })
    }
}
