export default function() {
    return {
        scope: {
            text: '@',
            onClick: '&'
        },
        templateUrl: 'view/shared/button.html'
    }
}