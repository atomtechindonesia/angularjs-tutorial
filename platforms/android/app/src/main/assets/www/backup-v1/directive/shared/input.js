export default function() {
    return {
        scope: {
            hint: '@',
            type: '@',
            ngModel: '=',
        },
        templateUrl: 'view/shared/input.html'
    }
}