import RepoUsers from '../repository/users.js'

export default function($scope) {
    $scope.screenVisible = false
    $scope.records = []

    $scope.$on('screenVisible-home', function() {
        $scope.screenVisible = true

        RepoUsers.GetTransaction()
            .then(res => {
                $scope.records = res
                $scope.$apply()
            })
    })

    $scope.openDetail = function(record) {
        alert(JSON.stringify(record))
        $scope.$broadcast('screenVisible-detail')
    }
}
