function Login(username, password) {
    return fetch(`https://ts-angularjs.herokuapp.com/login`, {
            method: 'POST',
            body: JSON.stringify({
                username,
                password
            })
        })
        .then(response => response.json())
        .then(json => {
            return {
                id: json.id,
                name: json.fullname
            }
        })
}

function GetTransaction() {
    return fetch(`https://ts-angularjs.herokuapp.com/transaction`)
        .then(response => response.json())
        .then(json => json)
}

export default {
    Login,
    GetTransaction,
}
