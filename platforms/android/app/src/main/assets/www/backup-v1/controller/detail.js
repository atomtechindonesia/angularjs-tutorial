export default function($scope) {
    $scope.screenVisible = false
    $scope.hideNavbar = false
    $scope.title = "Detail"

    $scope.$on('screenVisible-detail', function() {
        $scope.screenVisible = true
    })

    $scope.onBack = function() {
        $scope.screenVisible = false
    }
}
