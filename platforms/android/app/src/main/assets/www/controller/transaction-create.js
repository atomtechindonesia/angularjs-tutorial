app.controller('transactionCreateController', function($scope, $location) {
    $scope.form = {
        transaction_number: '',
        transaction_date:   new Date(),
        details:            []
    }
    $scope.handleBack = function() {
        $location.path('home')
    }

    $scope.handleSave = function() {
        console.log($scope.form)
    }

    $scope.handleAdd = function() {
        $scope.form.details.push({
            product:    '',
            quantity:   null,
            price:      null
        })
    }
})
