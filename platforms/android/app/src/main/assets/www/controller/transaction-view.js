app.controller('transactionViewController', function($scope, $location, $routeParams) {
    $scope.form = {
        transaction_number: '',
        transaction_date:   new Date(),
        details:            []
    }
    
    $scope.init = function() {
        alert($routeParams.id)
    }

    $scope.handleBack = function() {
        $location.path('home')
    }

    $scope.handleSave = function() {
        console.log($scope.form)
    }

    $scope.handleAdd = function() {
        $scope.form.details.push({
            product:    '',
            quantity:   null,
            price:      null
        })
    }
})
